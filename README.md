# Add new plugin

```
git submodule add "${GIT_URL}" "${VIM_START_FOLDER}/${PLUGIN_NAME}"

git commit -m "Added the submodule to the project."

git push
```

# Download all plugins

```
git submodule update --init --recursive
```

On vim:

```vim
call doge#install()
```

# Update all plugins

```
git submodule update --remote --merge
```

