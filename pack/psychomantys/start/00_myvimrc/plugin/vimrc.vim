" Ativa a cor
syntax on

" Seta o tipo de encode do arquivo
" setglobal fileencodings=utf-8
setglobal fileencoding=utf-8

" Desativa o mouse
setglobal mouse=

" Configurações que somente vão ser executadas no gvim
if has("gui_running")
	" ativa o mouse
	setglobal mouse=a
	" Le focus suit la souris
	setglobal mousef
	" Le bouton droit affiche une popup
	setglobal mousemodel=popup_setpos
	" seta background para dark
	highlight Normal guifg=White guibg=Black
	setglobal background=dark
	let &g:guifont="Liberation Mono 16"
else
	" Coisas que só são ativadas em modo texto
	" Set spell check sugesst to key <F8>
	map <F8> z=
	set termguicolors
endif

let g:NERDTreeGitStatusUntrackedFilesMode = 'all'

" Diz que o backspace apaga eol e começos de linha.
setglobal backspace=indent,eol,start

" Cofigurações do backup
" Estou usando backup diferencial com um plugin
" Descomente para não ter backup.
"set nobackup
" Seta o diretorio de backup. Isso é para o plugin de backup.
setglobal backupdir=~/.vim/backup//
" ativa o backup
setglobal backup
" Sufixo para o backup do plugin.
setglobal patchmode=.backup
" seta diretório de backup diferencial para o mesmo
" do backup normal
let g:savevers_dirs=&backupdir
let g:savevers_max=99

" A tecla F6 gera um código html colorido do arquivo atual.
map <F6> :runtime! syntax/2html.vim<CR>

" Cria documentação de uma função para o doxygen
imap <F12> <ESC>:Dox<CR>
map  <F12> :Dox<CR>

" Tecla que posta num site o arquivo atual e retorna a url do site
map  <F11> :Gist<CR>

autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif
set completeopt=longest,menuone 
filetype on
filetype plugin on
filetype indent on
filetype detect

" Personalizando as cores
" " Cores dos menus de autocompletar
highlight   Pmenu               ctermfg=White    ctermbg=Black         gui=NONE guifg=White    guibg=#600060
highlight   PmenuSel            ctermfg=Black    ctermbg=Gray          gui=NONE guifg=Black    guibg=White
highlight   PmenuSbar           ctermfg=Darkred  ctermbg=Darkmagenta   gui=NONE guifg=White    guibg=Black
" highlight   PmenuSbar           ctermfg=White    ctermbg=Black         gui=NONE guifg=White  guibg=Black
highlight   PmenuThumb          ctermfg=Cyan     ctermbg=yellow        gui=NONE guifg=Black    guibg=White
highlight   Folded              ctermfg=Cyan     ctermbg=DarkBlue      gui=NONE guifg=Darkgray guibg=#600060
highlight   SpecialKey          ctermfg=Grey                                    guifg=#600060
" Coluna vertical no local atual do cursor
highlight   CursorColumn                         ctermbg=Darkmagenta                           guibg=#303030
" Cores simbolos do gitgutter
highlight   SignColumn                           ctermbg=Black                                 guibg=Black
highlight   GitGutterAdd        ctermfg=Green    ctermbg=Black         gui=NONE guifg=Green    guibg=Black
highlight   GitGutterChange     ctermfg=Green    ctermbg=Black         gui=NONE guifg=Orange   guibg=Black
highlight   GitGutterDelete     ctermfg=Red      ctermbg=Black         gui=NONE guifg=Red      guibg=Black
" Coloca em baixo se tiver um erro de digitação
highlight   SpellBad            gui=undercurl term=underline cterm=underline ctermbg=NONE ctermul=NONE ctermul=Red guisp=Red

" Junta coluna de números com coluna de símbolos
set signcolumn=number

let g:gccsenseUseOmniFunc=1

" GIST plugin: if you want to detect filetype from filename...
let g:gist_detect_filetype = 1

" Autocomplete with tab
let g:SuperTabDefaultCompletionType = "<C-X><C-O>"

" Meu Ommin. Uso o omni apenas para completar :, . e ->
" Deu trabalho, mas copiei tudo para um arquivo.
let g:OmniCpp_MayCompleteDot = 1
let g:OmniCpp_MayCompleteArrow = 1
let g:OmniCpp_MayCompleteScope = 1

set noexpandtab
set ts=4
set foldmethod=indent
set nu

let g:load_doxygen_syntax=1

"if !exists("local_autocommands_loaded")
"	let local_autocommands_loaded=1

autocmd BufReadPost,BufNewFile *.thtml setfiletype html
au BufReadPost,BufNewFile *.log setlocal syntax=log

" Open all Fold
autocmd FileType html 1,$foldopen!
"endif

autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

let g:deoplete#enable_at_startup=1

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" yank to and paste from the clipboard without prepending "* to commands
let &clipboard = has('unnamedplus') ? 'unnamedplus' : 'unnamed'

" Windows like clipboard
imap <c-a> <esc>ggVG
nmap <c-a> ggVG
vm <c-x> "+x
vm <c-c> "+y
cno <c-v> <c-r>+
exe 'ino <script> <C-V>' paste#paste_cmd['i']

" Para acertar os erros quando vai formatar o texto
set shiftwidth=0

" Modificando o visual do tabs e espaços
"set list listchars=tab:»-,trail:·,extends:»,precedes:«
set list listchars=tab:\|\ ,trail:·,extends:»,precedes:«
set cursorcolumn

let g:python3_host_prog='/usr/bin/python3'
let g:python_host_prog='/usr/bin/python3'
let g:pyxversion=3

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" MULTIPURPOSE TAB KEY
" Indent if we're at the beginning of a line. Else, do completion.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! InsertTabWrapper()
	let col = col('.') - 1
	if !col || getline('.')[col - 1] !~ '\k'
		return "\<tab>"
	else
		return "\<c-p>"
	endif
endfunction
inoremap <expr> <tab> InsertTabWrapper()
inoremap <s-tab> <c-n>

" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion=['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion=['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType='<C-n>'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

set spell spelllang=pt

" YAML formater
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'
let g:ale_lint_on_text_changed = 'never'

" " ALE for checkov

function! Checkov(buffer, lines) abort
    " Matches patterns line the following:
    " /sonarqube.tf:95: CKV_AWS_79 Ensure Instance Metadata Service Version 1 is not enabled https://docs.bridgecrew.io/docs/bc_aws_general_31
    let l:pattern = '\v(.*):(\d*): (.*)'
    let l:output = []

    for l:match in ale#util#GetMatches(a:lines, l:pattern)
        let l:item = {
        \   'lnum': l:match[2],
        \   'col': 0,
        \   'text': l:match[3],
        \   'type': 'E',
        \}
        call add(l:output, l:item)
    endfor

    return l:output
endfunction


call ale#Set('yaml_checkov_executable', 'checkov')
call ale#Set('yaml_checkov_options', '--skip-check CKV2_K8S_6,CKV_K8S_31,CKV_K8S_40,CKV_K8S_22,CKV_K8S_35,CKV_K8S_28,CKV_K8S_38,CKV_K8S_43,CKV_K8S_37,CKV_K8S_21,CKV_K8S_15,CKV2_GHA_1')

function! Ale_linters_yaml_checkov_GetExecutable(buffer) abort
    return ale#Var(a:buffer, 'yaml_checkov_executable')
endfunction

function! Ale_linters_yaml_checkov_GetCommand(buffer) abort
    return '%e ' . '-f %t -o json --quiet ' . ale#Var(a:buffer, 'yaml_checkov_options')
endfunction

function! Ale_linters_yaml_checkov_Handle(buffer, lines) abort
    let l:output = []

    let l:results = get(get(ale#util#FuzzyJSONDecode(a:lines, {}), 'results', []), 'failed_checks', [])

    for l:violation in l:results
        call add(l:output, {
        \   'filename': l:violation['file_path'],
        \   'lnum': l:violation['file_line_range'][0],
        \   'end_lnum': l:violation['file_line_range'][1],
        \   'text': l:violation['check_name'] . ' [' . l:violation['check_id'] . ']',
        \   'detail': l:violation['check_id'] . ': ' . l:violation['check_name'] . "\n" .
        \             'For more information, see: '. l:violation['guideline'],
        \   'type': 'W',
        \   })
    endfor

    return l:output
endfunction

call ale#linter#Define('yaml', {
\   'name': 'checkov',
\   'output_stream': 'stdout',
\   'executable': function('Ale_linters_yaml_checkov_GetExecutable'),
\   'command': function('Ale_linters_yaml_checkov_GetCommand'),
\   'callback': 'Ale_linters_yaml_checkov_Handle',
\})


" " NERDComment
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
