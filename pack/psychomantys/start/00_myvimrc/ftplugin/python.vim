setlocal nu ts=4 noexpandtab foldmethod=indent

let g:ultisnips_python_style='sphinx'
let g:ultisnips_python_quoting_style='single'
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

let g:doge_doc_standard_python = 'sphinx'
let g:doge_python_settings = {
\  'single_quotes': 1,
\  'omit_redundant_param_types': 1
\}

" Cria documentação de uma função para o doxygen
imap <F12> <ESC>:DogeGenerate sphinx<CR>
map  <F12> :DogeGenerate sphinx<CR>
